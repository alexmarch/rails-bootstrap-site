class WelcomeController < ApplicationController
  def index
  	render :file => "welcome/index"
  end
  def signin
  	render :file => "welcome/signin"
  end
  def signup
  	render :file => "welcome/signup"
  end
end
