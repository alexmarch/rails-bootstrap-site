# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsBootstrapSite::Application.config.secret_key_base = '7597b0b7cab238669fdf5a44ba65422b36d92a82e67055a08b7804e2f74ede88aece265a9be03929876f9d53c085bdfcaa0569f6db547d903d0db18e7b0ced05'
